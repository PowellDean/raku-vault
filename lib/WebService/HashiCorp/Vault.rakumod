=begin pod
Copyright 2023 Dean Powell

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
=end pod

use WebService::HashiCorp::Vault::KeyStatus;
use WebService::HashiCorp::Vault::SecretV1;
use WebService::HashiCorp::Vault::SecretV2;
use WebService::HashiCorp::Vault::System;

use Cro::HTTP::Client;
use JSON::Tiny;

constant $defaultURL = 'http://127.0.0.1:8200';

enum Format <'hex' 'base64'>;

#| A client for communicating with the Vault API
class Client {
    has $!baseURL;
    has Str $!token;

    multi method baseURL {
        $!baseURL;
    }

    multi method baseURL($url) {
        $!baseURL = $url;
    }

    multi method token {
        $!token;
    }

    multi method token($aToken) {
        $!token = $aToken;
    }

    submethod BUILD(:$!baseURL, Str :$!token) {
        my (:$VAULT_ADDR, :$VAULT_TOKEN, *%) := %*ENV;
        unless defined $!baseURL {
            if defined $VAULT_ADDR {
                $!baseURL = $VAULT_ADDR;
            } else {
                $!baseURL ||= $defaultURL;
            }
        }

        if defined $VAULT_TOKEN { $!token = $VAULT_TOKEN; }
    }

    method deleteV1Secret(Str :$vault!, Str :$key!) {
        my $endpoint = "$!baseURL/v1/$vault/$key";

        my $response = await Cro::HTTP::Client.delete: $endpoint,
            content-type => 'application/json',
            headers => [
                X-Vault-Token => $!token
            ];
        CATCH {
            when X::Cro::HTTP::Error {
                say "Error Response: "
                    ~ .response.status
                    ~ " when performing POST on target "
                    ~ .request.target;
            }
        }

        $response.status;
    }

    method generateRandomBytes(Str :$format!, Int :$length!) {
        my $random = RandomData.new();
        my $gibberish = $random.generateRandomBytes(
            baseURL=>$!baseURL,
            tkn=>$!token,
            format=>$format,
            length=>$length
        );

        $gibberish;
    }

    #| Query for encryption key status
    method getEncryptionKeyStatus() {
        my $ks = KeyStatus.new();
        my $status = $ks.getKeyStatus(baseURL=>$!baseURL,
            tkn=>$!token);

        $status;
    }

    method getSecretV1(Str :$vault!, Str :$key!) {
        my $k1 = SecretV1.new();
        $k1.getSecret(
            baseURL=>$!baseURL,
            tkn=>$!token,
            vault=>$vault,
            key=>$key
        );

        $k1;
    }

    method getSecretV2(Str :$vault!, Str :$key!) {
        my $k1 = SecretV2.new();
        $k1.getSecret(
            baseURL=>$!baseURL,
            tkn=>$!token,
            vault=>$vault,
            key=>$key
        );

        $k1;
    }

    method getSystemHealth() {
        my $health = SystemHealth.new();
        $health.getSystemHealth(
            baseURL=>$!baseURL,
            tkn=>$!token
        );

        $health;
    }

    method isInitialized() returns Bool {
        my $endpoint = "$!baseURL/v1/sys/init";
        my $response = await Cro::HTTP::Client.get($endpoint);
        CATCH {
            when X::Cro::HTTP::Error {
                say "Error Response: "
                    ~ .response.status
                    ~ " when performing GET on target "
                    ~ .request.target;
            }
        }

        my $json = await $response.body;
        $json{ 'initialized' };
    }

    method isSealed() {
        my $endpoint = "$!baseURL/v1/sys/seal-status";
        my $client = Cro::HTTP::Client.new(
            headers => [
                X-Vault-Token => $!token
            ]);

        my $response = await $client.get($endpoint);
        CATCH {
            when X::Cro::HTTP::Error {
                say "Error Response: "
                    ~ .response.status
                    ~ " when performing GET on target "
                    ~ .request.target;
            }
        }

        my $json = await $response.body;
        $json{'sealed'}
    }

    method listV1Secrets(Str :$vault!) {
        my $k1 = SecretV1.new();
        $k1.listSecrets(
            baseURL=>$!baseURL,
            tkn=>$!token,
            vault=>$vault
        );
    }

    method listV2Secrets(Str :$vault!) {
        my $k1 = SecretV2.new();
        $k1.listSecrets(
            baseURL=>$!baseURL,
            tkn=>$!token,
            vault=>$vault
        );
    }

    method putSecretV1(Str :$vault!, Str :$key!, :%keyValues!) {
        my $endpoint = "$!baseURL/v1/$vault/$key";

        my $response = await Cro::HTTP::Client.post: $endpoint,
            content-type => 'application/json',
            body => %keyValues,
            headers => [
                X-Vault-Token => $!token
            ];
        CATCH {
            when X::Cro::HTTP::Error {
                say "Error Response: "
                    ~ .response.status
                    ~ " when performing POST on target "
                    ~ .request.target;
            }
        }

        $response.status;
    }

    method putSecretV2(Str :$vault!, Str :$key!, :%keyValues!) {
        my $endpoint = "$!baseURL/v1/$vault/data/$key";
        my %body = data => %keyValues;
        my $bodyStr = to-json %body;

        my $response = await Cro::HTTP::Client.post: $endpoint,
            content-type => 'application/json',
            body => $bodyStr,
            headers => [
                X-Vault-Token => $!token
            ];
        CATCH {
            when X::Cro::HTTP::Error {
                say "Error Response: "
                    ~ .response.status
                    ~ " when performing POST on target "
                    ~ .request.target;
                return False;
            }
        }

        my $json = await $response.body;
        $response.status;
    }

    multi method unseal(:@keys!) returns Bool {
        my ($stat, $result);

        for @keys -> $key {
            ($stat, $result) = self.unseal(key => $key);
            last unless ($stat == 200);
        }

        given $stat {
            when 200 {
                !$result{'sealed'}
            }
            default { False; }
        }
    }

    multi method unseal(Str :$key!) {
        my $endpoint = "$!baseURL/v1/sys/unseal";
        my %content = key => "$key";
        my $response = await Cro::HTTP::Client.post: $endpoint,
            content-type => 'application/json',
            body => %content;
        CATCH {
            when X::Cro::HTTP::Error {
                say "Error Response: "
                    ~ .response.status
                    ~ " when performing GET on target "
                    ~ .request.target;
            }
        }

        my $json = await $response.body;
        $response.status, $json;
    }
}
