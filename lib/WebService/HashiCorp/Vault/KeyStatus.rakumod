=begin pod
Copyright 2023 Dean Powell

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
=end pod

use Cro::HTTP::Client;

=begin pod
=begin head1
A Vault KeyStatus object
=end head1

=begin head2
Attributes:
=end head2

=defn encryptions
No clue what this actually is

=end pod

#| A KeyStatus is the object returned when querying for the status of
#| a given Vault key
class KeyStatus {
    has Int $!encryptions;
    has Str $!installTime;
    has Int $!leaseDuration;
    has Bool $!renewable;
    has Str $!requestId;

    multi method encryptions {
        $!encryptions;
    }

    multi method encryptions($aNumber) {
        $!encryptions = $aNumber;
    }

    multi method installTime {
        $!installTime;
    }

    multi method installTime($aString) {
        $!installTime = $aString
    }

    multi method leaseDuration {
        $!leaseDuration;
    }

    multi method leaseDuration($aNumber) {
        $!leaseDuration = $aNumber;
    }

    multi method renewable {
        $!renewable;
    }

    multi method renewable($aBool) {
        $!renewable = $aBool;
    }

    multi method requestId {
        $!requestId;
    }

    multi method requestId(Str $aString) {
        $!requestId = $aString;
    }

    submethod BUILD(
        :$!encryptions=0,
        :$!installTime='',
        :$!leaseDuration=0,
        :$!renewable=True,
        :$!requestId='') {
    }

    method getKeyStatus(Str :$baseURL!, Str :$tkn!) {
        my $endpoint = "$baseURL/v1/sys/key-status";
        my $client = Cro::HTTP::Client.new(
            headers => [
                X-Vault-Token => $tkn
            ]);

        my $response = await $client.get($endpoint);
        CATCH {
            when X::Cro::HTTP::Error {
                say "Error Response: "
                    ~ .response.status
                    ~ " when performing GET on target "
                    ~ .request.target;
            }
        }

        my $json = await $response.body;

        self.encryptions:   $json{'encryptions'};
        self.installTime:   $json{'install_time'};
        self.leaseDuration: $json{'lease_duration'};
        self.renewable:     $json{'renewable'};
        self.requestId:     $json{'request_id'};

        self;
    }
}
