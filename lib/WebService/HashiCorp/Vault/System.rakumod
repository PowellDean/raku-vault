=begin pod
Copyright 2023 Dean Powell

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
=end pod

use Cro::HTTP::Client;
use WebService::HashiCorp::Vault::ResponseHeader;

#| SystemHealth holds information about the health of the overall Vault
#| instance
class SystemHealth {
    has Str    $!clusterID;
    has Str    $!clusterName;
    has Bool   $!initialized;
    has Bool   $!performanceStandby;
    has Str    $!replicationDRMode;
    has Str    $!replicationPerformanceMode;
    has Bool   $!sealed;
    has Int    $!serverTimeUTC;
    has Bool   $!standby;
    has Str    $!version;

    multi method clusterID {
        $!clusterID;
    }

    multi method clusterID($aString) {
        $!clusterID = $aString;
    }

    multi method clusterName {
        $!clusterName;
    }

    multi method clusterName(Str $aString) {
        $!clusterName = $aString;
    }

    multi method initialized {
        $!initialized;
    }

    multi method initialized(Bool $trueOrFalse) {
        $!initialized = $trueOrFalse;
    }

    multi method performanceStandby {
        $!performanceStandby;
    }

    multi method performanceStandby(Bool $trueOrFalse) {
        $!performanceStandby = $trueOrFalse;
    }

    multi method replicationDRMode {
        $!replicationDRMode;
    }

    multi method replicationDRMode(Str $aString) {
        $!replicationDRMode = $aString;
    }

    multi method replicationPerformanceMode {
        $!replicationPerformanceMode;
    }

    multi method replicationPerformanceMode(Str $aString) {
        $!replicationPerformanceMode = $aString;
    }

    multi method sealed {
        $!sealed;
    }

    multi method sealed(Bool $trueOrFalse) {
        $!sealed = $trueOrFalse;
    }

    multi method serverTimeUTC {
        $!serverTimeUTC;
    }

    multi method serverTimeUTC(Int $aString) {
        $!serverTimeUTC = $aString;
    }

    multi method standby {
        $!standby;
    }

    multi method standby(Bool $trueOrFalse) {
        $!standby = $trueOrFalse;
    }

    multi method version {
        $!version;
    }

    multi method version(Str $aString) {
        $!version = $aString;
    }

    submethod BUILD(
        $!clusterID = '',
        $!clusterName = '',
        $!initialized = False,
        $!performanceStandby = False,
        $!replicationDRMode = '',
        $!replicationPerformanceMode = '',
        $!sealed = False,
        $!serverTimeUTC = 0,
        $!standby = False,
        $!version = '') {
    }

    method getSystemHealth(Str :$baseURL!, Str :$tkn!) {
        my $endpoint = "$baseURL/v1/sys/health";
        my $client = Cro::HTTP::Client.new(
            headers => [
                X-Vault-Token => $tkn
            ]);

        my $response = await $client.get($endpoint);
        CATCH {
            when X::Cro::HTTP::Error {
                say "Error Response: "
                    ~ .response.status
                    ~ " when performing GET on target "
                    ~ .request.target;
            }
        }

        my $json = await $response.body;
        self.clusterID:                  $json{'cluster_id'};
        self.clusterName:                $json{'cluster_name'};
        self.initialized:                $json{'initialized'};
        self.performanceStandby:         $json{'performance_standby'};
        self.replicationDRMode:          $json{'replication_dr_mode'};
        self.replicationPerformanceMode: $json{'replication_performance_mode'};
        self.sealed:                     $json{'sealed'};
        self.serverTimeUTC:              $json{'server_time_utc'};
        self.standby:                    $json{'standby'};
        self.version:                    $json{'version'};

        self;
    }
}

class RandomData {
    has ResponseHeader $!header;
    has Str $!data;

    multi method header {
        $!header;
    }

    multi method header(ResponseHeader $hdr!) {
        $!header = $hdr;
    }

    multi method data {
        $!data;
    }

    multi method data(Str $aString) {
        $!data = $aString;
    }

    submethod BUILD(
        :$!header=ResponseHeader.new();
        :$!data = "") {
    }

    method generateRandomBytes(Str :$baseURL!, Str :$tkn!, Str :$format!, Int :$length!){
        my $body = '{"format": "hex"}';
        my $bytesResponse = RandomData.new();

        given $format {
            when /base64/ {
                $body = '{"format": "base64"}';
            }
        }

        my $endpoint = "$baseURL/v1/sys/tools/random/platform/$length";
        my $client = Cro::HTTP::Client.new(
            headers => [
                X-Vault-Token => $tkn
            ]);
        my $response = await $client.post: $endpoint,
        #body => '{"format": "hex"}';
            body => $body;
        CATCH {
            when X::Cro::HTTP::Error {
                say "Error Response: "
                    ~ .response.status
                    ~ " when performing GET on target "
                    ~ .request.target;
            }
        }

        my $json = await $response.body;
        self.header: ResponseHeader.new().fromJson(json => $json);
        self.data: $json{'data'}{'random_bytes'};

        self;
    }
}
