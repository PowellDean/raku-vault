=begin pod
Copyright 2023 Dean Powell

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
=end pod

use Cro::HTTP::Client;
use WebService::HashiCorp::Vault::ResponseHeader;

class SecretV2 {
    has ResponseHeader $!header;
    has     %!data;
    has Str $!createdTime;
    has     %!customMetadata;
    has Str $!deletionTime;
    has Bool $!destroyed;
    has Int $!version;

    multi method header {
        $!header;
    }

    multi method header(ResponseHeader $hdr) {
        $!header = $hdr;
    }

    multi method data {
        %!data;
    }

    multi method data(%kvPairs!) {
        %!data = %kvPairs;
    }

    multi method createdTime {
        $!createdTime;
    }

    multi method createdTime(Str $aUTCTime) {
        $!createdTime = $aUTCTime
    }

    multi method customMetadata {
        %!customMetadata
    }

    multi method customMetadata(%metadata) {
        %!customMetadata = %metadata
    }

    multi method deletionTime {
        $!deletionTime;
    }

    multi method deletionTime(Str $aUTCTime) {
        $!deletionTime = $aUTCTime
    }

    multi method destroyed {
        $!destroyed;
    }

    multi method destroyed(Bool $anIndicator) {
        $!destroyed = $anIndicator;
    }

    multi method version{
        $!version;
    }

    multi method version(Int $aVersionNumber) {
        $!version = $aVersionNumber;
    }

    submethod BUILD(
        :$!header = ResponseHeader.new();
        :%!data = {},
        :$!createdTime='',
        :%!customMetadata={},
        :$!deletionTime='',
        :$!destroyed=False,
        :$!version=0) {
    }

    method getSecret(Str :$baseURL!, Str :$tkn!, Str :$vault!, Str :$key!) {
        my $endpoint = "$baseURL/v1/$vault/data/$key";
        my $client = Cro::HTTP::Client.new(
            headers => [
                X-Vault-Token => $tkn
            ]);

        my $response = await $client.get($endpoint);
        CATCH {
            when X::Cro::HTTP::Error {
                say "Error Response: "
                    ~ .response.status
                    ~ " when performing GET on target "
                    ~ .request.target;
            }
        }

        my $json = await $response.body;
        self.header: ResponseHeader.new().fromJson(json => $json);
        self.createdTime:   $json{'data'}{'metadata'}{'created_time'};
        self.deletionTime:  $json{'data'}{'metadata'}{'deletion_time'};
        self.destroyed:     $json{'data'}{'metadata'}{'destroyed'};
        self.version:       $json{'data'}{'metadata'}{'version'};

        self.header: ResponseHeader.new().fromJson(json => $json);
        self.data: $json{'data'}{'data'};

        if keys $json{'data'}{'metadata'}{'custom_metadata'} {
            self.customMetadata =
                    $json{'data'}{'metadata'}{'custom_metadata'};
        }

        self;
    }

    method listSecrets(Str :$baseURL!, Str :$tkn!, Str :$vault!) {
        my $endpoint = "$baseURL/v1/$vault?list=true";
        my $client = Cro::HTTP::Client.new(
            headers => [
                X-Vault-Token => $tkn
            ]);

        my $response = await $client.get($endpoint);
        CATCH {
            when X::Cro::HTTP::Error {
                say "Error Response: "
                    ~ .response.status
                    ~ " when performing GET on target "
                    ~ .request.target;
            }
        }

        my $json = await $response.body;
        my @secrets = list($json{'data'}{'keys'});

        @secrets;
    }
}
