=begin pod
Copyright 2023 Dean Powell

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
=end pod

use Cro::HTTP::Client;
use WebService::HashiCorp::Vault::ResponseHeader;

#| A version 1 Secret is just a list of Key/Value pairs
class SecretV1 {
    has ResponseHeader $!header;
    has     %!data;

    multi method header {
        $!header;
    }

    multi method header(ResponseHeader $hdr!) {
        $!header = $hdr;
    }

    multi method data {
        %!data;
    }

    multi method data(%kvPairs!) {
        %!data = %kvPairs;
    }

    submethod BUILD(
        :$!header=ResponseHeader.new();
        :%!data = {}) {
    }

    #| Retrieve the secret key $key, which should be found in the mountpoint
    #| $vault
    method getSecret(Str :$baseURL!, Str :$tkn!, Str :$vault!, Str :$key!) {
        my $endpoint = "$baseURL/v1/$vault/$key";
        my $client = Cro::HTTP::Client.new(
            headers => [
                X-Vault-Token => $tkn
            ]);

        my $response = await $client.get($endpoint);
        CATCH {
            when X::Cro::HTTP::Error {
                say "Error Response: "
                    ~ .response.status
                    ~ " when performing GET on target "
                    ~ .request.target;
            }
        }

        my $json = await $response.body;
        self.header: ResponseHeader.new().fromJson(json => $json);
        self.data: $json{'data'};

        self;
    }

    method dataKeys {
        self.data.keys;
    }

    method listSecrets(Str :$baseURL!, Str :$tkn!, Str :$vault!) {
        my $endpoint = "$baseURL/v1/$vault?list=true";
        my $client = Cro::HTTP::Client.new(
            headers => [
                X-Vault-Token => $tkn
            ]);

        my $response = await $client.get($endpoint);
        CATCH {
            when X::Cro::HTTP::Error {
                say "Error Response: "
                    ~ .response.status
                    ~ " when performing GET on target "
                    ~ .request.target;
            }
        }

        my $json = await $response.body;
        my @secrets = list($json{'data'}{'keys'});

        @secrets;
    }
}
